function s() {
  var renderer = new THREE.WebGLRenderer({ alpha: true, canvas: document.getElementById('myCanvas') })
  renderer.setClearColor( 0x000000, 0 )
  renderer.setSize( window.innerWidth, window.innerHeight )
  // document.body.appendChild( renderer.domElement )

  var scene = new THREE.Scene();
  var camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 )
  var controls = new THREE.OrbitControls(camera)
  var pauseSimulation = false
  var frameCount = 0
  var texture = new THREE.TextureLoader().load( 'themes/ppia-rmit/assets/images/star.png' );

  window.addEventListener('resize', function () {
    camera.aspect = window.innerWidth / window.innerHeight
    camera.updateProjectionMatrix()

    renderer.setSize( window.innerWidth, window.innerHeight )
  })

  controls.enabled = false
  controls.autoRotate = true
  controls.autoRotateSpeed = 0.05

  camera.position.set( 0, 20, 100 )
  scene.rotation.z = -0.261799
  controls.update();

  if (!window.particleSystem) {
      window.particleSystem = undefined
  }

  function changeShape (random, size) {
    let functions = [
      pickCubePoint,
      pickRandomCubeSlice,
      pickCubeEdge,
    ]

    return functions[random % functions.length](size)
  }

  particleSystem = new ParticleSystem({
    renderer: renderer,
    scene: scene,
    camera: camera,
    particleCount: 500,
    spawnAlgorithm: ParticleSystem.spawmShape.SPHERE({
      r: 15
    }),
    particleMaterial: function () {
      let colors = [0x895F6A, 0x77534E, 0x765681, 0x847784, 0xB68C8D, 0xA997A6]
      let index = Math.floor(Math.random() * Math.floor(colors.length))
      let defaultMaterial = new THREE.SpriteMaterial({
        color: colors[index],
        map: texture,
        blending: THREE.AdditiveBlending,
        transparent: true,
        opacity: 0.7
      })

      return defaultMaterial
    },
    particleSetting: {
      updates: (p, r) => {
        let time = frameCount - p.life
        let scale = (Math.cos(Math.cos(time) + Math.sin(2 * time)) + 1.2)

        r.scale.x = scale
        r.scale.y = scale
        r.scale.z = scale

        let distance = p.position.distanceTo(p.targetPosition)
        // console.log(distance);
        if (distance < 1) {
          p.velocity.x = 0
          p.velocity.y = 0
          p.velocity.z = 0

          p.position = p.targetPosition
        } else {
          p.velocity.subVectors(p.targetPosition, p.position).divideScalar(distance / 0.01)
        }
      },
      onAfterCreate: function (particle, renderer) {
        particle.life = __HELPERS__.getRandomArbitrary(-10,10)

        particle.targetPosition = new THREE.Vector3(0, 0, 0)
        distance = particle.position.distanceTo(particle.targetPosition)
        particle.velocity = new THREE.Vector3(0, 0, 0)

        setInterval(function () {
          let point = ParticleSystem.spawmShape.SPHERE({
            r: 200
          })

          particle.targetPosition = new THREE.Vector3(point.x, point.y, point.z)
        }, 5000)
      }
    }
  })
  window.particleSystem = particleSystem

  setInterval(function () {
    frameCount += 0.001
  }, 60/1000)

  function animate() {
    if (!pauseSimulation) {
      requestAnimationFrame( animate )
      particleSystem.update()
      controls.update()
    	renderer.render( scene, camera )
    }
  }
  requestAnimationFrame( animate )
}
function reload() {if (typeof window.ParticleSystem === 'undefined') {setTimeout(reload, 550)}else{s()}}reload()
