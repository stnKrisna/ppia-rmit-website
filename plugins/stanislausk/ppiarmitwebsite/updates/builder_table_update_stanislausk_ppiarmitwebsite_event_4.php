<?php namespace Stanislausk\PpiaRmitWebsite\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateStanislauskPpiarmitwebsiteEvent4 extends Migration
{
    public function up()
    {
        Schema::table('stanislausk_ppiarmitwebsite_event', function($table)
        {
            $table->string('read_more_link', 512);
            $table->string('read_more_label', 50);
        });
    }
    
    public function down()
    {
        Schema::table('stanislausk_ppiarmitwebsite_event', function($table)
        {
            $table->dropColumn('read_more_link');
            $table->dropColumn('read_more_label');
        });
    }
}
