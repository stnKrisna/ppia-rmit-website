<?php namespace Stanislausk\PpiaRmitWebsite\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateStanislauskPpiarmitwebsiteEvent extends Migration
{
    public function up()
    {
        Schema::create('stanislausk_ppiarmitwebsite_event', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('event_name', 140);
            $table->dateTime('event_date');
            $table->dateTime('hidden_at');
            $table->boolean('headline');
            $table->string('short_description', 255);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('stanislausk_ppiarmitwebsite_event');
    }
}
