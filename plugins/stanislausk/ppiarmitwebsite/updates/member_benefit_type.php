<?php namespace Stanislausk\PpiaRmitWebsite\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class MemberBenefitType extends Migration
{
    public function up()
    {
      Schema::create('stanislausk_ppiarmitwebsite_member_benefit_type', function($table)
      {
          $table->engine = 'InnoDB';
          $table->increments('id')->unsigned();
          $table->string('type_name', 255);
          $table->integer('display_order')->nullable(false)->default(0);
          $table->timestamp('created_at')->nullable();
          $table->timestamp('updated_at')->nullable();
          $table->timestamp('deleted_at')->nullable();
      });

      Schema::table('stanislausk_ppiarmitwebsite_member_benefit', function($table)
      {
          $table->integer('benefit_type_id')->nullable(false);
      });
    }

    public function down()
    {
        Schema::dropIfExists('stanislausk_ppiarmitwebsite_member_benefit_type');

        Schema::table('stanislausk_ppiarmitwebsite_member_benefit', function($table)
        {
            $table->dropColumn('benefit_type_id');
        });
    }
}
