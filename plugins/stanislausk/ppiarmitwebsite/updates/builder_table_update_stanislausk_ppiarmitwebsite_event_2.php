<?php namespace Stanislausk\PpiaRmitWebsite\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateStanislauskPpiarmitwebsiteEvent2 extends Migration
{
    public function up()
    {
        Schema::table('stanislausk_ppiarmitwebsite_event', function($table)
        {
            $table->boolean('headline')->default(0)->change();
        });
    }
    
    public function down()
    {
        Schema::table('stanislausk_ppiarmitwebsite_event', function($table)
        {
            $table->boolean('headline')->default(null)->change();
        });
    }
}
