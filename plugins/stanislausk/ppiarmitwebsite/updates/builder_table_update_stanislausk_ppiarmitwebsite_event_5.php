<?php namespace Stanislausk\PpiaRmitWebsite\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateStanislauskPpiarmitwebsiteEvent5 extends Migration
{
    public function up()
    {
        Schema::table('stanislausk_ppiarmitwebsite_event', function($table)
        {
            $table->string('read_more_color', 7);
        });
    }
    
    public function down()
    {
        Schema::table('stanislausk_ppiarmitwebsite_event', function($table)
        {
            $table->dropColumn('read_more_color');
        });
    }
}
