<?php namespace Stanislausk\PpiaRmitWebsite\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateStanislauskPpiarmitwebsiteInternalEvent extends Migration
{
    public function up()
    {
        Schema::create('stanislausk_ppiarmitwebsite_internal_event', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('event_name', 255);
            $table->string('event_description', 500);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('stanislausk_ppiarmitwebsite_internal_event');
    }
}
