<?php namespace Stanislausk\PpiaRmitWebsite\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateStanislauskPpiarmitwebsiteEvent3 extends Migration
{
    public function up()
    {
        Schema::table('stanislausk_ppiarmitwebsite_event', function($table)
        {
            $table->string('short_description', 255)->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('stanislausk_ppiarmitwebsite_event', function($table)
        {
            $table->string('short_description', 255)->nullable(false)->change();
        });
    }
}
