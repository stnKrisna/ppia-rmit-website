<?php namespace Stanislausk\PpiaRmitWebsite\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateStanislauskPpiarmitwebsiteCommitteeRole extends Migration
{
    public function up()
    {
        Schema::create('stanislausk_ppiarmitwebsite_committee_role', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('role_name', 255);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('stanislausk_ppiarmitwebsite_committee_role');
    }
}
