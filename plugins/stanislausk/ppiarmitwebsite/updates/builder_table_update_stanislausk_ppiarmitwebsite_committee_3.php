<?php namespace Stanislausk\PpiaRmitWebsite\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateStanislauskPpiarmitwebsiteCommittee3 extends Migration
{
    public function up()
    {
        Schema::table('stanislausk_ppiarmitwebsite_committee', function($table)
        {
            $table->renameColumn('role', 'member_role');
        });
    }
    
    public function down()
    {
        Schema::table('stanislausk_ppiarmitwebsite_committee', function($table)
        {
            $table->renameColumn('member_role', 'role');
        });
    }
}
