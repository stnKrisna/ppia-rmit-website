<?php namespace Stanislausk\PpiaRmitWebsite\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateStanislauskPpiarmitwebsiteLeadershipHistory extends Migration
{
    public function up()
    {
        Schema::create('stanislausk_ppiarmitwebsite_leadership_history', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 64);
            $table->string('year', 4);
            $table->string('uni_major', 64);
            $table->string('leadership_style', 20);
            $table->text('vision');
            $table->text('highlight_of_the_year');
            $table->string('quote', 280);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('stanislausk_ppiarmitwebsite_leadership_history');
    }
}
