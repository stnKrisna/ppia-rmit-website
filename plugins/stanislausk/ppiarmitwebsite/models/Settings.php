<?php namespace Stanislausk\PpiaRmitWebsite\Models;

use October\Rain\Database\Model;

/**
 * PPIA RMIT settings model
 *
 * @package system
 * @author Stanislaus Krisna
 *
 */
class Settings extends Model
{
    use \October\Rain\Database\Traits\Validation;

    public $implement = ['System.Behaviors.SettingsModel'];

    public $settingsCode = 'stanislausk_ppiarmit_settings';

    public $settingsFields = 'fields.yml';

    public $attachOne = [
        'gapi_key' => ['System\Models\File', 'public' => false]
    ];


    /**
     * Validation rules
     */
    public $rules = [];
}
