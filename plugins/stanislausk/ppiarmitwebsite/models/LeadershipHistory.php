<?php namespace Stanislausk\PpiaRmitWebsite\Models;

use Model;

/**
 * Model
 */
class LeadershipHistory extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'stanislausk_ppiarmitwebsite_leadership_history';

    protected $jsonable = ['highlight_of_the_year', 'vision'];

    public $attachOne = [
        'profilePicture' => 'System\Models\File',
        'profileBanner'  => 'System\Models\File',
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
