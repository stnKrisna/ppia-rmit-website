<?php namespace Stanislausk\PpiaRmitWebsite\Models;

use Model;

/**
 * Model
 */
class InternalEvent extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /* Relations */
    public $attachOne = [
      'event_cover' => 'System\Models\File'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'stanislausk_ppiarmitwebsite_internal_event';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
