<?php namespace Stanislausk\PpiaRmitWebsite\Models;

use Model;

/**
 * Model
 */
class MemberBenefitType extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'stanislausk_ppiarmitwebsite_member_benefit_type';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
