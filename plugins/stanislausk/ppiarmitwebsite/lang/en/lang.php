<?php return [
    'plugin' => [
        'name' => 'PPIA RMIT Website',
        'description' => '',
    ],
    'benefit' => [
        'manage_member_benefit' => 'Manage Member Benefit',
    ],
    'stanislausk' => [
        'ppiarmitwebsite::lang' => [
            'benefit' => [
                'manage_member_benefit' => 'Manage Member Benefit',
            ],
        ],
    ],
];