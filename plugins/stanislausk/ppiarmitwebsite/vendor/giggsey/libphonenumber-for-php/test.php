<?php

require_once 'vendor/autoload.php';

function parsePhoneNumber ($input) {
	$phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
	$acceptedCountry = ['AU', 'ID'];

	for ($i = 0; $i < count($acceptedCountry); ++$i) {
		try {
			$candidateNum = $phoneUtil->parse($input, $acceptedCountry[$i]);

			// Validate number. Must pass all validation to be selected.
			$isPossibleNumber = $phoneUtil->isPossibleNumber($candidateNum);
			$isValidNumber = $phoneUtil->isValidNumber($candidateNum);
			$isValidNumberForRegion = $phoneUtil->isValidNumberForRegion($candidateNum, $acceptedCountry[$i]);
			if ($isPossibleNumber && $isValidNumber && $isValidNumberForRegion) {
				return $candidateNum;
			}
		} catch (\libphonenumber\NumberParseException $e) {}
	}

	throw new Exception('Invalid phone number');
}

$phoneNum = '+6281273954927';
try {
	$phoneNumber = parsePhoneNumber($phoneNum);
	var_dump($phoneNumber);
	$phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
	echo $phoneUtil->format($phoneNumber, \libphonenumber\PhoneNumberFormat::INTERNATIONAL);
} catch (Exception $e) {
	echo 'Invalid number';
}
