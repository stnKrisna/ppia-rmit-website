<?php
require_once 'google/recaptcha/src/autoload.php';

function validateCaptcha($gRecaptchaResponse)
{
    /* Return parameters */
    $errorCode = -1;
    $errorMessage = 'Error displaying error ¯\_(ツ)_/¯';

    /* Captcha configuration */
    $secret = '6LfY358UAAAAAEQUgpUjFxBl1LSlvKUNw6H8gtea'; // Shh
    // $secret = '6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe'; // Debug
    $remoteIp = $_SERVER['REMOTE_ADDR'];
    $recaptcha = new \ReCaptcha\ReCaptcha($secret);

    $resp = $recaptcha->setExpectedHostname($_SERVER['HTTP_HOST'])
              ->verify($gRecaptchaResponse, $remoteIp);
    if ($resp->isSuccess()) {
        $errorCode = 0;
        $errorMessage = 'success';
    } else {
        $errors = $resp->getErrorCodes();
        switch ($errors[0]) {
          case 'missing-input-secret':
            $errorCode = 1;
            $errorMessage = 'Invalid captcha configuration';
          break;
          case 'invalid-input-secret':
            $errorCode = 2;
            $errorMessage = 'Invalid captcha configuration';
          break;
          case 'missing-input-response':
            $errorCode = 3;
            $errorMessage = 'Please complete the captcha';
          break;
          case 'invalid-input-response':
            $errorCode = 4;
            $errorMessage = 'Invalid captcha';
          break;
          case 'bad-request':
            $errorCode = 5;
            $errorMessage = 'Captcha error';
          break;
          case 'timeout-or-duplicate':
            $errorCode = 6;
            $errorMessage = 'Old captcha or duplicate captcha. Please try again...';
          break;
          case 'hostname-mismatch':
            $errorCode = 7;
            $errorMessage = 'Invalid website';
          break;
          default:
            // Only god (and google) knows how we ended up here...
            break;
        }
    }

    return [
      'code' => $errorCode,
      'message' => $errorMessage
    ];
}
