<?php namespace Stanislausk\Ppiarmitwebsite\Components;

use Cms\Classes\ComponentBase;
use Stanislausk\Ppiarmitwebsite\models\InternalEvent;

class InternalEventComponent extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Internal Event',
            'description' => 'PPIA RMIT Internal event display'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getInternalEvents()
    {
        $ret = [];

        foreach (InternalEvent::all() as $event) {
            $event_push = [
              'event_name' => $event->event_name,
              'event_cover' => null,
              'event_description' => $event->event_description
            ];

            if (isset($event->event_cover)) {
                $event_push['event_cover'] = $event->event_cover->getPath();
            }

            array_push($ret, $event_push);
        }

        return $ret;
    }
}
