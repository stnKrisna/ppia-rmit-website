<?php namespace Stanislausk\Ppiarmitwebsite\Components;

use Cms\Classes\ComponentBase;
use Stanislausk\Ppiarmitwebsite\models\LeadershipHistory;

class LeadershipHistoryComponent extends ComponentBase
{
    private $presidents = null;
    
    public function componentDetails()
    {
        return [
            'name'        => 'Leadership History',
            'description' => 'PPIA RMIT Leadership history component'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
    
    public function init () {
        if ($this->presidents == null) {
            $this->presidents = LeadershipHistory::all();
        }
    }
    
    public function getPresidents () {
        return $this->presidents;
    }
}
