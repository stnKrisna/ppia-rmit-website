<?php namespace Stanislausk\Ppiarmitwebsite\Components;

use Cms\Classes\ComponentBase;
use Stanislausk\Ppiarmitwebsite\models\MemberBenefit;

class MemberBenefitComponent extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Member Benefit',
            'description' => 'Display member benefit'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getBenefit()
    {
        $ret = [];

        $query = MemberBenefit::join(
            'stanislausk_ppiarmitwebsite_member_benefit_type',
            'stanislausk_ppiarmitwebsite_member_benefit_type.id',
            '=',
            'stanislausk_ppiarmitwebsite_member_benefit.benefit_type_id'
        )
        ->select([
          'stanislausk_ppiarmitwebsite_member_benefit.*',
          'stanislausk_ppiarmitwebsite_member_benefit_type.type_name',
          'stanislausk_ppiarmitwebsite_member_benefit_type.display_order',
          ])
        ->orderBy('display_order');

        foreach ($query->get() as $benefit) {
            if (!isset($ret[$benefit->benefit_type_id])) {
              $ret[$benefit->benefit_type_id] = [];
            }

            $benefit_push = [
              'brand_name' => $benefit->brand_name,
              'brand_logo' => null,
              'benefit_description' => $benefit->benefit_description,
              'brand_address' => $benefit->brand_address,
              'brand_phone' => $benefit->brand_phone,
              'brand_website' => $benefit->brand_website,
              'benefit_type' => $benefit->benefit_type_id,
              'benefit_type_name' => $benefit->type_name
            ];

            if (isset($benefit->brandLogo)) {
                $benefit_push['brand_logo'] = $benefit->brandLogo->getPath();
            }

            array_push($ret[$benefit->benefit_type_id], $benefit_push);
        }

        return $ret;
    }
}
