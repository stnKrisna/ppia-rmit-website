<?php namespace Stanislausk\PpiaRmitWebsite\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class BenefitTypeController extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'stanislausk.ppiarmitwebsite.manage_member_benefit' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Stanislausk.PpiaRmitWebsite', 'plugin-home', 'side-menu-item');
    }
}
