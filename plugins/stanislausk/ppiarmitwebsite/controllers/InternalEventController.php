<?php namespace Stanislausk\PpiaRmitWebsite\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class InternalEventController extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController',        'Backend\Behaviors\ReorderController'    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = [
        'stanislausk.ppiarmitwebsite.manage_internal_event'
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Stanislausk.PpiaRmitWebsite', 'plugin-home', 'internal-event-menu-item');
    }
}
